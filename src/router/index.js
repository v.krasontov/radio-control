import Vue from 'vue'
import VueRouter from 'vue-router'
import control from '../control.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'control',
    component: control
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
