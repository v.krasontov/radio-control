const autoprefixer = require('autoprefixer')

module.exports = {
  devServer: {
    disableHostCheck: true
  },
  pwa: {
    themeColor: '#000000',
    msTileColor: '#00aba9',
    appleMobileWebAppCapable: 'yes',
    iconPaths: {
      favicon32: 'favicon-32x32.png',
      favicon16: 'favicon-16x16.png',
      appleTouchIcon: 'apple-touch-icon.png',
      maskIcon: 'safari-pinned-tab.svg',
      msTileImage: 'mstile-150x150.png'
    }
  },
	css: {
    loaderOptions: {
      sass: {
        implementation: require('sass'),
        sassOptions: {
          includePaths: ['./node_modules']
        },
      }
    }
	}
}
